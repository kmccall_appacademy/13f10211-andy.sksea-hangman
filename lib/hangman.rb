class Hangman
  attr_reader :guesser, :referee, :board
  
  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end
  
  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end
  
  def take_turn
    g = @guesser.guess(@board)
    res = @referee.check_guess(g)
    # Both guesser and board need to "handle" the response.
    @guesser.handle_response(g, res)
    update_board(res, g)
  end
  
  def display_board
    puts @board.map { |c| c.nil? ? "_" : c }.join
  end
  
  private
  
  def update_board(res, g)
    res.each { |i| @board[i] ||= g }
  end
    
end

class HumanPlayer
  attr_reader :characters_remaining, :secret_length
  
  def initialize
    @characters_remaining = [*'a'..'z']
  end
  
  def guess(board)
    print "> "
    $stdout.flush
    gets.chomp
  end
  
  def handle_response(char, res)
    @characters_remaining.reject! { |c| c == char } 
  end
  
  def register_secret_length(num)
    @secret_length = num
    puts "Secret length: #{@secret_length}"
  end
  
end

class ComputerPlayer
  attr_reader :dictionary, :candidate_words
  
  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
  end
  
  def pick_secret_word
    @secret = @dictionary[Random.new.rand(0...@dictionary.size)]
    # puts "secret: #{@secret}"
    @secret.length
  end
  
  def register_secret_length(num)
    # if computer is guesser
    @secret_length = num
    @candidate_words.select! { |w| w.length == num }
  end
  
  def check_guess(char)
    raise ArgumentError, "Only guess characters" if char.length > 1
    # returns an array of indices where character is found in the secret,
    # or empty array if none are present.
    @secret.indice_of(char)
  end
  
  def guess(board=nil)
    most_common_candidate_char(board)
  end
  
  # Filter candidate words using guess and response indices.
  def handle_response(char, res)
    # @characters_remaining.reject! { |c| c == char }
    @candidate_words.select! { |w| w.indice_of(char) == res }
  end
  
  def validate_secret(str)
    @secret == str
  end
  
  private
  
  # Chooses the most common character from words remaining in @candidate words to use at guess.
  def most_common_candidate_char(board=nil)
    char_count = Hash.new(0)
    @candidate_words.each do |w|
      w.chars.each { |c| char_count[c] += 1 }
    end
    if board
      # If a board exists, avoid guessing characters already revealed on the board.
      board_chars = board.compact.uniq
      char_count.reject! { |k, _| board_chars.include?(k) }
    end
    char_count.max_by { |_, v| v }[0]
  end
  
end

class String
  def indice_of(char)
    self.chars.map.with_index { |c, i| c == char ? i : nil }.compact
  end
end
