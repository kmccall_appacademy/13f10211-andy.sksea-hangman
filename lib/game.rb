require_relative 'hangman'
require 'pry'

class Game
  
  def initialize
    path = File.join(File.dirname(__FILE__), "dictionary.txt")
    @dict = read_file_to_arr(path)
    @referee = ComputerPlayer.new(@dict)
    @guesser = ComputerPlayer.new(@dict)
    # @guesser = HumanPlayer.new
    @players = { referee: @referee, guesser: @guesser }
    @h = Hangman.new(@players)
    @h.setup
  end
  
  def start
    while @h.board.include?(nil)
      @h.display_board
      @h.take_turn 
    end 
    if @referee.validate_secret(@h.board.join)
      puts "Congrats! You guessed the correct word: #{@h.board.join}."
    else
      raise StandardError, "Game ended with board not matching referee's secret."
    end
  end
  
  def read_file_to_arr(path)
    dictionary = []
    File.open(path) do |f|
      f.each_line do |line|
        dictionary << line.gsub("\n", "")
      end
    end
    dictionary
  end
  
end

g = Game.new
g.start
